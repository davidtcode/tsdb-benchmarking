## tsdb-benchmarking

What follows are some preliminary performance tests, using the benchmarking tool JMH, of two open-source time-series databases (Influxdb and Kairosdb). These tests are a subset of a wider examination of what time-series database (or some equivalent) could be used in a proposed IOT application.

Although the actual practicalities of working with these databases, and coming to understand their limitations, has shown that both are unsuitable for our needs, I'll present the results for completeness.

All the following tests were run on a single-node Amazon m3.medium EC2 instance running on Ubuntu 14.04.3 LTS (GNU/Linux 3.13.0-61-generic x86_64) with Intel(R) Xeon(R) CPU E5-2670 v2 @ 2.50GHz and 3.75 GiB DIMM RAM.

### The fictitious data model

For the purposes of these benchmarks, we'll imagine we're collecting sensor data from an airplane. A sensor records the speed of the craft (call it a metric or a point) and each reading (or datapoint) has the following associated information: a value (the speed), a timestamp, the type of plane and the type of flight. The latter two are considered *metadata* or *tags* and they help group or categorise the datapoint. 

### How to run the tests

Clone the repository and build the source: (You'll need Java 1.8, Maven and Git.)

* Clone the repository
* Change directory to base of the repository
* Run *mvn clean install*

Then install and configure the time-series databases which the tests are (currently) executed against:

* Influxdb (https://influxdb.com/)
* Kairosdb (http://kairosdb.github.io/)
    * Kairosdb is built upon Apache Cassandra so this will need to be installed as well.
  
For these tests, the default out-of-the-box installation was used; only the logging for both was disabled.

Update *benchmark.properties* so that the urls, port numbers, etc. match those for the configured databases. Assuming this file is updated correctly, that the databases are installed, configured and started, the benchmarks can be executed with the general JMH command:

```
java -jar target/benchmarks.jar -foe true
```

This will run the benchmarks with the default JMH settings. 

## Write tests

### Write test: The "single point" (non-batched) test

Here we measure the throughput performance of a single, non-batched write of a datapoint. The metric has the name "planeSpeed", the value is a random long, the timestamp is the current time in milliseconds, the plane type is "Airbus A320" and the flight type is "Commercial". 

```
Test 1 (single threaded):

java -jar target/benchmarks.jar -wi 20 -i 500 -r 1 -f 5 -foe true

(20 warm up iterations, 500 iterations lasting a second, repeated/forked 5 times, and failing with any errors.)

Benchmark                           Mode   Cnt   Score   Error    Units
InfluxdbBenchmark.writeSinglePoint  thrpt  2500  248.011 ± 2.338  ops/s
KairosdbBenchmark.writeSinglePoint  thrpt  2500  627.514 ± 5.540  ops/s
```
So, in the case of Influxdb, the average throughput is approximately 248 operations per second, with an error of ± 2.338 (which tells us how consistent the results where or what the standard deviation is). That was a single-threaded benchmark and Kairosdb is significantly faster. With 2 threads the results were:

```
Test 2 (2 threads):

java -jar target/benchmarks.jar -wi 20 -i 500 -r 1 -f 5 -t2 -foe true

Benchmark                           Mode   Cnt   Score    Error     Units
InfluxdbBenchmark.writeSinglePoint  thrpt  2500  435.906  ± 4.421   ops/s
KairosdbBenchmark.writeSinglePoint  thrpt  2500  1249.479 ± 12.095  ops/s
```

Unfortunately, the Influxdb Java client suffers from timeouts when the benchmark is executed with anything more than 2 threads. As it's not immediately obvious how this can be fixed, this is a significant drawback. With 10 threads, Kairosdb performs as follows:

```
Test 3 (10 threads, Kairosdb only):

java -jar target/benchmarks.jar -wi 20 -i 500 -r 1 -f 5 -t10 -foe true

Benchmark                           Mode   Cnt   Score    Error     Units
KairosdbBenchmark.writeSinglePoint  thrpt  2500  1321.293 ± 11.406  ops/s

```

## Read/query tests

For the "read" benchmarks, 100k mock sensor readings (see *PlaneDataQueryHelper*) are added to the database as part of the setup. Within these, there are 5 different plane types, each of which has a defined range of values and timestamps. (Again, see *PlaneDataQueryHelper* for details.)

### Read/query test: Query metric, filtered by tag

In sql-parlance, the query is essentially of the form "select * from planeSpeed where plane='Boeing 707'". That is, find all the datapoints which have a plane type of 'Boeing 707', as defined in its tag or meta data.

```
Test 4 (single thread)
java -jar benchmarks.jar -bm avgt -wi 20 -i 500 -r 1 -f 2 -foe true

Benchmark                         Mode   Cnt  Score   Error  Units
InfluxdbBenchmark.queryAcrossTag  avgt  1000  0.465 ± 0.004   s/op
KairosdbBenchmark.queryAcrossTag  avgt  1000  0.268 ± 0.003   s/op
```

Here we're measuring the average response time, as opposed to the throughput. As before, Kairosdb is slightly faster. With 2 threads, the response time is longer. (Again, the Influxdb client suffers from a timeout failure, with anything above 2 threads.)

```
Test 5 (2 threads)
java -jar benchmarks.jar -bm avgt -wi 20 -i 500 -r 1 -f 2 t -2 -foe true

Benchmark                         Mode   Cnt  Score   Error  Units
InfluxdbBenchmark.queryAcrossTag  avgt  1000  0.913 ± 0.007   s/op
KairosdbBenchmark.queryAcrossTag  avgt  1000  0.573 ± 0.008   s/op
```

### Read/query test: Query metric, filtered by date range

Here we're looking for all the datapoints over a certain timestamp range. Something of the form: "select * from planeSpeed where time >= start_time_ms and time <= end_time_ms"

```
Test 6 (single thread)
java -jar benchmarks.jar -bm avgt -wi 20 -i 500 -r 1 -f 2 -foe true

Benchmark                               Mode   Cnt  Score   Error  Units
InfluxdbBenchmark.queryAcrossDateRange  avgt  1000  0.455 ± 0.003   s/op
KairosdbBenchmark.queryAcrossDateRange  avgt  1000  0.259 ± 0.003   s/op
```

And with 2 threads:

```
Test 7 (2 threads)
java -jar benchmarks.jar -bm avgt -wi 20 -i 500 -r 1 -f 2 t -10 -foe true

Benchmark                               Mode   Cnt  Score   Error  Units
InfluxdbBenchmark.queryAcrossDateRange  avgt  1000  0.943 ± 0.005   s/op
KairosdbBenchmark.queryAcrossDateRange  avgt  1000  0.523 ± 0.007   s/op
```

As throughout these tests, the performance of Kairosdb is (comparatively speaking) faster than Influxdb.

### Final thoughts

Ultimately, these tests have (directly and indirectly) shown that both Influxdb and Kairosdb aren't suitable for our needs. 

Firstly, I encountered two problems with Influxdb. One, the timeout issue mentioned above. Two, the write performance would, intermittently, drop off dramatically. This, it would appear, is a JBD I/O related issue, the "fix" for which isn't clear.

Secondly, although it's performance wasn't overly bad (given the modest spec. of the EC2 instance) the querying capability of Kairosdb (having now used it) probably won't be rich and flexible enough, in the long run. For instance, it isn't currently capable of executing queries across value ranges; that is, of finding all the datapoints whose values are between such and such figures. If we are going to use some application (such as Kariosdb) as a means of facilitating and simplifying access to Apache Cassandra, then it ought to be more feature rich.

...

The next step is to examine Cassandra coupled with (for starters) Apache Spark, in the hope that the latter can provide the desired richer analytic data processing (OLAP) functionality. And to do so in a multiple-node environment in order to make the tests more realistic.