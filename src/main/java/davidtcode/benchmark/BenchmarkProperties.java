package davidtcode.benchmark;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads and provides all the properties required for the benchmark tests.
 * 
 * @author David.Tegart
 */
public class BenchmarkProperties {

	private static final Logger LOG = LoggerFactory
		.getLogger(BenchmarkProperties.class.getCanonicalName());

	public static final String FILE_PATH = "benchmark.properties";
	
	private static Properties properties = null;
	static {

		FileInputStream inputStream = null;
		try {
			
			inputStream = new FileInputStream(FILE_PATH);
			
			properties = new Properties();
			properties.load(inputStream);

		} catch (Exception ex) {

			LOG.error("Cannot load the properties file " + FILE_PATH);
			
			properties = null; // Let NPE fail any dependent tests

		} finally {

			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException ex) {
					LOG.error("Cannot close input stream", ex);
				}
			}
		}
	}

	public static String prop(String key) {
		return properties.getProperty(key);
	}
}