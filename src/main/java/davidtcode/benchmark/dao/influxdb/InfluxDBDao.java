package davidtcode.benchmark.dao.influxdb;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.Validate;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;

/**
 * Simple Dao class for reading/writing to InfluxDb for the purposes of the 
 * benchmark tests.
 * 
 * @author David.Tegart
 */
public class InfluxDBDao {

	private InfluxDB influxDB;
	
	private String url;
	private String username;
	private String password;
	private String database;
	private String retentionPolicy;

	public InfluxDBDao(
			String url, 
			String username, 
			String password, 
			String database,
			String retentionPolicy) {

		this.url = url;
		this.username = username;
		this.password = password;
		this.database = database;
		this.retentionPolicy = retentionPolicy;

		init();
	}
	
	public void init() {
		influxDB = InfluxDBFactory.connect(url, username, password);
	}

	public Point writePoint(
			String measurement, 
			String planeTypeTagValue, 
			String flightTypeTagValue,
			long timestamp,
			TimeUnit precisionToSet,
			long value){

		Point point = Point
				.measurement(measurement)
				.tag("plane", planeTypeTagValue)
				.tag("flight", flightTypeTagValue)
				.time(timestamp, precisionToSet)
				.field("value", value)
				.build();

		influxDB.write(database, retentionPolicy, point);

		return point;
	}
	
	public QueryResult query(String query) {
		
		Validate.notEmpty(query);
		
		return influxDB.query(new Query(query, this.database));
	}

	public void dropDatabase(String databaseName) {
		
		Validate.notEmpty(databaseName);
		
		influxDB.deleteDatabase(databaseName);
	}

	public void createDatabase(String databaseName) {
		
		Validate.notEmpty(databaseName);
		
		influxDB.createDatabase(databaseName);
	}
}