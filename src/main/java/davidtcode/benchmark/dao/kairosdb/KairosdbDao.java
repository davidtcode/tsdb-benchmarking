package davidtcode.benchmark.dao.kairosdb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Date;

import org.apache.commons.lang3.Validate;
import org.kairosdb.client.HttpClient;
import org.kairosdb.client.builder.MetricBuilder;
import org.kairosdb.client.builder.QueryBuilder;
import org.kairosdb.client.response.QueryResponse;
import org.kairosdb.client.response.Response;

/**
 * Simple Dao class for reading/writing to Kairosdb for the purposes of the 
 * benchmark tests.
 * 
 * @author David.Tegart
 */
public class KairosdbDao {

	HttpClient client = null;
	
	private String url;

	public KairosdbDao(String url) throws MalformedURLException {
		
		this.url = url;
		
		init();
	}
	
	public void init() throws MalformedURLException {
		client = new HttpClient(url);
	}
	
	public Response writePoint(
			String metric, 
			String planeTypeTagValue, 
			String flightTypeTagValue,
			long timestamp,
			long value) throws URISyntaxException, IOException {
		
		Validate.notEmpty(metric);
		Validate.notEmpty(planeTypeTagValue);
		Validate.notEmpty(flightTypeTagValue);
		Validate.isTrue(timestamp > 0);
		
		MetricBuilder builder = MetricBuilder.getInstance();
		
		builder
			.addMetric(metric)
			.addTag("plane", planeTypeTagValue)
			.addTag("flight", flightTypeTagValue)
			.addDataPoint(timestamp, value);

		return client.pushMetrics(builder);
	}

	public void deleteMetrc(String metric) 
			throws URISyntaxException, IOException {

		Validate.notEmpty(metric);
		
		client.deleteMetric(metric);
	}

	public QueryResponse query(Date startDate, Date endDate, String metric, 
			String planeTypeTagValue) 
			throws URISyntaxException, IOException {

		Validate.notNull(startDate);
		Validate.notEmpty(metric);

		QueryBuilder builder = QueryBuilder.getInstance();

		builder.setStart(startDate);

		if(endDate != null) {
			builder.setEnd(endDate);
		}
		
		if(planeTypeTagValue != null) {
			builder.addMetric(metric).addTag("plane", planeTypeTagValue);
		} else {
			builder.addMetric(metric);
		}

		return client.query(builder);
	}

	public void shutDown() throws IOException {
		client.shutdown();
	}
}