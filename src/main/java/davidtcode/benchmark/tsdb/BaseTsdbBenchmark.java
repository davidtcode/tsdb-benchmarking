package davidtcode.benchmark.tsdb;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import davidtcode.benchmark.BenchmarkProperties;

/**
 * @author David.Tegart
 */
public abstract class BaseTsdbBenchmark {

	@State(Scope.Benchmark)
	public static class GeneralState {
		
		public String metric;
		public String planeTypeTagValue;
		public String flightTypeTagValue;
		public RandomLongRing random;
		
		@Setup(Level.Trial)
		public void setUp() {
			random = new RandomLongRing();
			
			metric = BenchmarkProperties.prop("metric.name");
			planeTypeTagValue = BenchmarkProperties.prop("metric.planeTypeTagValue");
			flightTypeTagValue = BenchmarkProperties.prop("metric.flightTypeTagValue");
		}
		
		public long getNextRandomLong() {
			return random.next();
		}
	}
}

class RandomLongRing {
	
	private AtomicInteger currentIndex = new AtomicInteger(0);
	private int maxRandomValues = 10000;
	private int highestValuesArrayIndex = maxRandomValues - 1;
	
	private final long[] values = new long [maxRandomValues];
	
	public RandomLongRing () {
		
		Random random = new Random();
		for(int i = 0; i < maxRandomValues; i++) {
			values[i] = random.nextLong();
		}
	}
	
	public long next() {
		return next(currentIndex.getAndIncrement());
	}
	
	private long next(int localIndex) { // Using a local to avoid IndexOutOfBoundsException
		
		if(localIndex > highestValuesArrayIndex) {
			currentIndex.set(0);
			return values[0];
		}
		else {
			currentIndex.incrementAndGet();
			return values[localIndex];
		}
	}
}