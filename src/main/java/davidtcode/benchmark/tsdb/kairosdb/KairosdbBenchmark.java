package davidtcode.benchmark.tsdb.kairosdb;

import org.apache.commons.lang3.Validate;
import org.kairosdb.client.response.QueryResponse;
import org.kairosdb.client.response.Response;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

import davidtcode.benchmark.BenchmarkProperties;
import davidtcode.benchmark.dao.kairosdb.KairosdbDao;
import davidtcode.benchmark.tsdb.BaseTsdbBenchmark;
import davidtcode.benchmark.tsdb.PlaneDataPoint;
import davidtcode.benchmark.tsdb.PlaneDataQueryHelper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains the benchmark tests for Kairosdb.
 * 
 * @author David.Tegart
 */
public class KairosdbBenchmark extends BaseTsdbBenchmark {

	private static final Logger LOG = LoggerFactory
			.getLogger(KairosdbBenchmark.class.getCanonicalName());

	@State(Scope.Benchmark)
	public static class KairosdbDaoState {

		KairosdbDao kairosdbDao;

		@Setup(Level.Trial)
		public void setUp() throws MalformedURLException {
			kairosdbDao = new KairosdbDao(
					BenchmarkProperties.prop("kairosdb.url"));
		}

		@TearDown(Level.Trial)
		public void tearDown() throws IOException {
			kairosdbDao.shutDown();
		}
	}

	@State(Scope.Benchmark)
	public static class StateForQueryTest {
		
		List<PlaneDataPoint> dataPoints;
		KairosdbDao kairosdbDao;
		String metric;
		String planeTypeTagValue;
		String flightTypeTagValue;

		QueryResponse queryResponse;

		@Setup(Level.Trial)
		public void setUp() throws URISyntaxException, IOException {

			dataPoints = PlaneDataQueryHelper.getStateForQuery();
			metric = BenchmarkProperties.prop("metric.name");
			
			kairosdbDao = new KairosdbDao(
					BenchmarkProperties.prop("kairosdb.url"));

			populateReadDatabase();
		}
		
		private void populateReadDatabase() 
				throws URISyntaxException, IOException {
			
			kairosdbDao.deleteMetrc(metric);

			LOG.debug("populateReadDatabase Writing test data, this could take several minutes .......");
			
			for(PlaneDataPoint point : dataPoints) {
				kairosdbDao.writePoint(
						point.getMetric(), 
						point.getPlaneTypeTagValue(), 
						point.getFlightTypeTagValue(), 
						point.getTimestamp(), 
						point.getValue());
			}
		}

		@TearDown(Level.Trial)
		public void tearDown() throws IOException {

			long numberOfResults = queryResponse
					.getQueries()
					.get(0)
					.getSampleSize();

			Validate.isTrue(numberOfResults == PlaneDataQueryHelper.DATAPOINTS_PER_TAG,
					"Expected "+PlaneDataQueryHelper.DATAPOINTS_PER_TAG+" results, got " + numberOfResults);
		}
	}

	@Benchmark()
	@Fork(2)
	@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 100, time = 1, timeUnit = TimeUnit.SECONDS)
	public Response writeSinglePoint(KairosdbDaoState dao, GeneralState state) 
			throws URISyntaxException, IOException {

		return dao.kairosdbDao.writePoint(
				state.metric, 
				state.planeTypeTagValue, 
				state.flightTypeTagValue, 
				System.currentTimeMillis(), 
				state.getNextRandomLong()); // Avoid dead-code elimination by returning something
	}

	@Benchmark()
	@Fork(2)
	@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 100, time = 1, timeUnit = TimeUnit.SECONDS)
	public QueryResponse queryAcrossTag(StateForQueryTest queryState) 
			throws URISyntaxException, IOException {

		QueryResponse response = queryState.kairosdbDao.query(
				new Date(PlaneDataQueryHelper.TS_START_MS), 
				new Date(PlaneDataQueryHelper.TS_END_MS), 
				queryState.metric, PlaneDataQueryHelper.PLANE_TYPE_BOEING);
		
		queryState.queryResponse = response;
		
		return response;
	}

	@Benchmark()
	@Fork(2)
	@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 100, time = 1, timeUnit = TimeUnit.SECONDS)
	public QueryResponse queryAcrossDateRange(StateForQueryTest queryState) 
			throws URISyntaxException, IOException {

		QueryResponse response = queryState.kairosdbDao.query(
				new Date(PlaneDataQueryHelper.PLANE_TYPE_DOUGLAS_TS_START_MS), 
				new Date(PlaneDataQueryHelper.PLANE_TYPE_DOUGLAS_TS_END_MS), 
				queryState.metric, null);
		
		queryState.queryResponse = response;
		
		return response;
	}
}