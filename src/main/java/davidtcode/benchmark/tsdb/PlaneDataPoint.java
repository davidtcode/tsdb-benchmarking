package davidtcode.benchmark.tsdb;

import java.util.concurrent.TimeUnit;

/**
 * @author David.Tegart
 */
public class PlaneDataPoint {

	private String metric;
	private String planeTypeTagValue;
	private String flightTypeTagValue;
	private long timestamp;
	private TimeUnit timestampPrecision;
	private long value;

	public PlaneDataPoint(String metric, String planeTypeTagValue,
			String flightTypeTagValue, long timestamp,
			TimeUnit timestampPrecision, long value) {

		this.metric = metric;
		this.planeTypeTagValue = planeTypeTagValue;
		this.flightTypeTagValue = flightTypeTagValue;
		this.timestamp = timestamp;
		this.timestampPrecision = timestampPrecision;
		this.value = value;
	}

	public String getMetric() {
		return metric;
	}

	public String getPlaneTypeTagValue() {
		return planeTypeTagValue;
	}

	public String getFlightTypeTagValue() {
		return flightTypeTagValue;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public TimeUnit getTimestampPrecision() {
		return timestampPrecision;
	}

	public long getValue() {
		return value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PlaneDataPoint [metric=");
		builder.append(metric);
		builder.append(", planeTypeTagValue=");
		builder.append(planeTypeTagValue);
		builder.append(", flightTypeTagValue=");
		builder.append(flightTypeTagValue);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append(", timestampPrecision=");
		builder.append(timestampPrecision);
		builder.append(", value=");
		builder.append(value);
		builder.append("]");
		return builder.toString();
	}
}