package davidtcode.benchmark.tsdb;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import davidtcode.benchmark.BenchmarkProperties;

/**
 * @author David.Tegart
 */
public class PlaneDataQueryHelper {

	public static final String PLANE_TYPE_AIRBUS = "Airbus A380";
	public static final String PLANE_TYPE_BOEING = "Boeing 707";
	public static final String PLANE_TYPE_BERIEV = "Beriev Be-200";
	public static final String PLANE_TYPE_TUPOLEV = "Tupolev Tu-154";
	public static final String PLANE_TYPE_DOUGLAS = "Douglas DC-3";

	public static final int PLANE_TYPE_AIRBUS_LOW_VALUE = 100;
	public static final int PLANE_TYPE_AIRBUS_HIGH_VALUE = 199;
	public static final int PLANE_TYPE_BOEING_LOW_VALUE = 200;
	public static final int PLANE_TYPE_BOEING_HIGH_VALUE = 299;
	public static final int PLANE_TYPE_BERIEV_LOW_VALUE = 300;
	public static final int PLANE_TYPE_BERIEV_HIGH_VALUE = 399;
	public static final int PLANE_TYPE_TUPOLEV_LOW_VALUE = 400;
	public static final int PLANE_TYPE_TUPOLEV_HIGH_VALUE = 499;
	public static final int PLANE_TYPE_DOUGLAS_LOW_VALUE = 500;
	public static final int PLANE_TYPE_DOUGLAS_HIGH_VALUE = 599;

	// Epoch ms for start and end of March 2014
	public static final long PLANE_TYPE_DOUGLAS_TS_START_MS = 1393632000000L;
	public static final long PLANE_TYPE_DOUGLAS_TS_END_MS = 1396310399000L;
	public static final String PLANE_TYPE_DOUGLAS_TS_START = PLANE_TYPE_DOUGLAS_TS_START_MS + "ms";
	public static final String PLANE_TYPE_DOUGLAS_TS_END = PLANE_TYPE_DOUGLAS_TS_END_MS + "ms";

	// Epoch ms start and end for full date range
	public static final long TS_START_MS = 1383264000000L; // 1st Nov 2013 thru to ..
	public static final long TS_END_MS = 1396310399000L; // end of Mar 2014 

	public static final int DATAPOINTS_PER_TAG = Integer
			.parseInt(BenchmarkProperties.prop("query.number.datapointsPerPlaneTag"));

	private static List<PlaneDataPoint> dataPoints;
	static {

		/*
		 * The data will consist of DATAPOINTS_PER_TAG x 5 data-points in total.
		 * There will be 5 different types within that total.
		 * 
		 * The metric is the speed of a plane. The flight-type metadata for each
		 * measurement will be the same, namely "Commercial". The plane-type
		 * metadata will range over 5 different aircraft types. The values (i.e.
		 * the speed) for each will, in turn, vary over 5 different ranges,
		 * similarly with the timestamps.
		 * 
		 * This predictable variation should allow us to run queries over the
		 * data and get predictable results.
		 * 
		 * Flight       Plane type      Value    Timestamp range  Percision
		 * ---------------------------------------------------------------------
		 * "Commercial" Airbus A380     100-199  In Nov, 2013     Millisecond
		 * "Commercial" Boeing 707      200-299  In Dec, 2013     Millisecond
		 * "Commercial" Beriev Be-200   300-399  In Jan, 2014     Millisecond
		 * "Commercial" Tupolev Tu-154  400-499  In Feb, 2014     Millisecond
		 * "Commercial" Douglas DC-3    500-599  In Mar, 2014     Millisecond
		 */

		dataPoints = new ArrayList<>();

		long novEpochMs = LocalDateTime
				.of(2013, Month.NOVEMBER, 1, 0, 0)
				.atZone(ZoneId.systemDefault())
				.toInstant()
				.toEpochMilli();
		long decEpochMs = LocalDateTime
				.of(2013, Month.DECEMBER, 1, 0, 0)
				.atZone(ZoneId.systemDefault())
				.toInstant()
				.toEpochMilli();
		long janEpochMs = LocalDateTime
				.of(2014, Month.JANUARY, 1, 0, 0)
				.atZone(ZoneId.systemDefault())
				.toInstant()
				.toEpochMilli();
		long febEpochMs = LocalDateTime
				.of(2014, Month.FEBRUARY, 1, 0, 0)
				.atZone(ZoneId.systemDefault())
				.toInstant()
				.toEpochMilli();
		long marEpochMs = LocalDateTime
				.of(2014, Month.MARCH, 1, 0, 0)
				.atZone(ZoneId.systemDefault())
				.toInstant()
				.toEpochMilli();

		for (int i = 0; i < DATAPOINTS_PER_TAG; i++) {

			dataPoints.add(new PlaneDataPoint(BenchmarkProperties
					.prop("metric.name"), PLANE_TYPE_AIRBUS,
					BenchmarkProperties.prop("metric.flightTypeTagValue"),
					ThreadLocalRandom.current().nextLong(novEpochMs, novEpochMs + 2332800000L), 
					TimeUnit.MILLISECONDS,
					ThreadLocalRandom.current().nextLong(PLANE_TYPE_AIRBUS_LOW_VALUE, PLANE_TYPE_AIRBUS_HIGH_VALUE)));

			dataPoints.add(new PlaneDataPoint(BenchmarkProperties
					.prop("metric.name"), PLANE_TYPE_BOEING,
					BenchmarkProperties.prop("metric.flightTypeTagValue"),
					ThreadLocalRandom.current().nextLong(decEpochMs, decEpochMs + 2332800000L), 
					TimeUnit.MILLISECONDS,
					ThreadLocalRandom.current().nextLong(PLANE_TYPE_BOEING_LOW_VALUE, PLANE_TYPE_BOEING_HIGH_VALUE)));

			dataPoints.add(new PlaneDataPoint(BenchmarkProperties
					.prop("metric.name"), PLANE_TYPE_BERIEV,
					BenchmarkProperties.prop("metric.flightTypeTagValue"),
					ThreadLocalRandom.current().nextLong(janEpochMs, janEpochMs + 2332800000L), 
					TimeUnit.MILLISECONDS,
					ThreadLocalRandom.current().nextLong(PLANE_TYPE_BERIEV_LOW_VALUE, PLANE_TYPE_BERIEV_HIGH_VALUE)));

			dataPoints.add(new PlaneDataPoint(BenchmarkProperties
					.prop("metric.name"), PLANE_TYPE_TUPOLEV,
					BenchmarkProperties.prop("metric.flightTypeTagValue"),
					ThreadLocalRandom.current().nextLong(febEpochMs, febEpochMs + 2332800000L), 
					TimeUnit.MILLISECONDS,
					ThreadLocalRandom.current().nextLong(PLANE_TYPE_TUPOLEV_LOW_VALUE, PLANE_TYPE_TUPOLEV_HIGH_VALUE)));

			dataPoints.add(new PlaneDataPoint(BenchmarkProperties
					.prop("metric.name"), PLANE_TYPE_DOUGLAS,
					BenchmarkProperties.prop("metric.flightTypeTagValue"),
					ThreadLocalRandom.current().nextLong(marEpochMs, marEpochMs + 2332800000L), 
					TimeUnit.MILLISECONDS,
					ThreadLocalRandom.current().nextLong(PLANE_TYPE_DOUGLAS_LOW_VALUE, PLANE_TYPE_DOUGLAS_HIGH_VALUE)));
		}

		// Ensure the datapoints have a randomness about them
		Collections.shuffle(dataPoints, new Random(System.nanoTime()));
	}

	public static List<PlaneDataPoint> getStateForQuery() {
		return dataPoints;
	}
}