package davidtcode.benchmark.tsdb.influxdb;

import org.apache.commons.lang3.Validate;
import org.influxdb.dto.Point;
import org.influxdb.dto.QueryResult;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;

import davidtcode.benchmark.BenchmarkProperties;
import davidtcode.benchmark.dao.influxdb.InfluxDBDao;
import davidtcode.benchmark.tsdb.BaseTsdbBenchmark;
import davidtcode.benchmark.tsdb.PlaneDataPoint;
import davidtcode.benchmark.tsdb.PlaneDataQueryHelper;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains the benchmarks tests for Influxdb.
 * 
 * @author David.Tegart
 */
public class InfluxdbBenchmark extends BaseTsdbBenchmark {

	private static final Logger LOG = LoggerFactory
			.getLogger(InfluxdbBenchmark.class.getCanonicalName());

	@State(Scope.Benchmark)
	public static class InfluxDBDaoState {
		InfluxDBDao influxDBDao = new InfluxDBDao(
				BenchmarkProperties.prop("influxdb.url"),
				BenchmarkProperties.prop("influxdb.username"),
				BenchmarkProperties.prop("influxdb.password"),
				BenchmarkProperties.prop("influxdb.writeTest.database"),
				BenchmarkProperties.prop("influxdb.retentionPolicy"));
	}

	@State(Scope.Benchmark)
	public static class StateForQueryTest {
		
		List<PlaneDataPoint> dataPoints;
		InfluxDBDao influxDBDao;
		String metric;
		String planeTypeTagValue;
		String flightTypeTagValue;
		String queryDatabaseName;
		
		String queryByPlaneTag;
		String queryAcrossValueRange;
		String queryAcrossDateRange;
		QueryResult queryResult;

		@Setup(Level.Trial)
		public void setUp() {

			dataPoints = PlaneDataQueryHelper.getStateForQuery();
			metric = BenchmarkProperties.prop("metric.name");
			
			influxDBDao = new InfluxDBDao(
					BenchmarkProperties.prop("influxdb.url"),
					BenchmarkProperties.prop("influxdb.username"),
					BenchmarkProperties.prop("influxdb.password"),
					BenchmarkProperties.prop("influxdb.readTest.database"),
					BenchmarkProperties.prop("influxdb.retentionPolicy"));

			populateReadDatabase(BenchmarkProperties.prop("influxdb.readTest.database"));
			
			generateQueries();
		}
		
		private void populateReadDatabase(String databaseName) {
			
			influxDBDao.dropDatabase(databaseName);
			influxDBDao.createDatabase(databaseName);
			
			LOG.debug("populateReadDatabase Writing test data, this could take several minutes .......");
			
			for(PlaneDataPoint point : dataPoints) {
				influxDBDao.writePoint(
						point.getMetric(),
						point.getPlaneTypeTagValue(),
						point.getFlightTypeTagValue(),
						point.getTimestamp(), 
						point.getTimestampPrecision(),
						point.getValue());
			}
		}
		
		private void generateQueries() {

			queryByPlaneTag = new StringBuilder()
				.append("select * FROM \"")
				.append(this.metric)
				.append("\" where plane='")
				.append(PlaneDataQueryHelper.PLANE_TYPE_BOEING)
				.append("'")
				.toString();
			
			queryAcrossValueRange = new StringBuilder()
				.append("select * FROM \"")
				.append(this.metric)
				.append("\" where value >=")
				.append(PlaneDataQueryHelper.PLANE_TYPE_TUPOLEV_LOW_VALUE)
				.append(" and value < ")
				.append(PlaneDataQueryHelper.PLANE_TYPE_TUPOLEV_HIGH_VALUE)
				.toString();

			queryAcrossDateRange = new StringBuilder()
				.append("select * FROM \"")
				.append(this.metric)
				.append("\" where time >=")
				.append(PlaneDataQueryHelper.PLANE_TYPE_DOUGLAS_TS_START)
				.append(" and time <= ")
				.append(PlaneDataQueryHelper.PLANE_TYPE_DOUGLAS_TS_END)
				.toString();
		}
		
		@TearDown(Level.Trial)
		public void tearDown() {

			int actualNumberOfResults = queryResult
					.getResults()
					.get(0)
					.getSeries()
					.get(0)
					.getValues()
					.size();

			Validate.isTrue(actualNumberOfResults == PlaneDataQueryHelper.DATAPOINTS_PER_TAG,
					"Expected "+PlaneDataQueryHelper.DATAPOINTS_PER_TAG+" results, got " + actualNumberOfResults);
		}
	}

	@Benchmark()
	@Fork(2)
	@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 100, time = 1, timeUnit = TimeUnit.SECONDS)
	public Point writeSinglePoint(InfluxDBDaoState dao, GeneralState state) {

		return dao.influxDBDao.writePoint(
				state.metric,
				state.planeTypeTagValue,
				state.flightTypeTagValue,
				System.currentTimeMillis(), 
				TimeUnit.MILLISECONDS,
				state.getNextRandomLong()); // Avoid dead-code elimination by returning something
	}
	
	@Benchmark()
	@Fork(2)
	@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 100, time = 1, timeUnit = TimeUnit.SECONDS)
	public QueryResult queryAcrossTag(StateForQueryTest queryState) {

		QueryResult result = queryState.influxDBDao.query(queryState.queryByPlaneTag);

		queryState.queryResult= result;

		return result;
	}
	
	@Benchmark()
	@Fork(2)
	@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 100, time = 1, timeUnit = TimeUnit.SECONDS)
	public QueryResult queryAcrossValueRange(StateForQueryTest queryState) {

		QueryResult result = queryState.influxDBDao.query(queryState.queryAcrossValueRange);

		queryState.queryResult= result;

		return result;
	}

	@Benchmark()
	@Fork(2)
	@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 100, time = 1, timeUnit = TimeUnit.SECONDS)
	public QueryResult queryAcrossDateRange(StateForQueryTest queryState) {

		QueryResult result = queryState.influxDBDao.query(queryState.queryAcrossDateRange);

		queryState.queryResult= result;

		return result;
	}
}